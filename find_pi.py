# coin_flip.py
# Programmer: Demar Brown (Demar Codes)
# Date: Dec 24, 2020
# Program Details: This program prompts the user to enter a number (N) from 1 to 15.
    #The program then prints the value of pi to N number of decimal places.
    #The program neither rounds pi up nor down 

# Complete: Note the range 1-15 is not enforced. For future challenge implement a user defined exception for 
# range 1-15

#-----------------------------------------------

#Importing python's random library
from math import pi

def print_pi(num_decimals):
    pi_value = str(pi)
    required_decimals = []
    final_pi = ''
    
    #Seperating pi value into two parts before and after the decimal point and selecting index 1 as a single 
    # element in a listlist
    pi_decimal= pi_value.split('.')[1]
    pi_decimal= pi_decimal.split()
    
    #Seperating each digit in the new list pi_decimal as indivdual members of a new list
    for digit in pi_decimal[0]:
        required_decimals.append(digit)

    #Selecting required aspects of decimals per function parameter
    required_decimals = required_decimals[0:num_decimals].copy()
    
    #Joining list into string an reattaching first part of pi value
    final_pi = final_pi.join(required_decimals)
    final_pi = '3.' + final_pi
    
    print(f"\nPi to {num_decimals} decimal places is: {final_pi}")    


#Program begins here

print("\n--- PI to the N--- \n")

num_decimals = int(input("Enter the number of decimal places for pi: "))
print_pi(num_decimals)